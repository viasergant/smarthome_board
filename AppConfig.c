#include <AppConfig.h>

SemaphoreHandle_t serialPortMutex;

/*******************************************************************************
 * Function Name  : SysTick_Configuration
 * Description    : Configures the SysTick clocks.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/

void vApplicationStackOverflowHook() {

}
void SysTick_Configuration(void) {

}

void SetPortDirectionIn(GPIO_TypeDef* port, uint16_t pin, int pull) {
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = 1 << pin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

	if (pull < 0) {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	} else if (pull == 0) {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	} else if (pull > 0) {
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	}

	GPIO_Init(port, &GPIO_InitStructure);
}

void SetPortDirectionOut(GPIO_TypeDef* port, uint16_t pin) {

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = 1 << pin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;

	GPIO_Init(port, &GPIO_InitStructure);
}

uint16_t getAdcValue() {
	ADC_SoftwareStartConvCmd(ADC2, ENABLE);
	while (ADC_GetFlagStatus(ADC2, ADC_FLAG_EOC) == RESET)
		;
	return ADC_GetConversionValue(ADC2);
}

void SetupGPIO() {
	RCC_APB2PeriphClockCmd(
			RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC
					| RCC_APB2Periph_ADC2, ENABLE);
	/*
	 GPIO_InitTypeDef  GPIO_InitStructure;

	 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0
	 | GPIO_Pin_1
	 | GPIO_Pin_2
	 | GPIO_Pin_3
	 | GPIO_Pin_4
	 | GPIO_Pin_5
	 | GPIO_Pin_6
	 | GPIO_Pin_7
	 | GPIO_Pin_8
	 | GPIO_Pin_11
	 | GPIO_Pin_12;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	 GPIO_Init(GPIOA, &GPIO_InitStructure);
	 GPIO_InitStructure.GPIO_Pin   =   GPIO_Pin_1
	 | GPIO_Pin_3
	 | GPIO_Pin_4
	 | GPIO_Pin_5
	 | GPIO_Pin_6
	 | GPIO_Pin_7
	 | GPIO_Pin_8
	 | GPIO_Pin_9
	 | GPIO_Pin_10
	 | GPIO_Pin_11
	 | GPIO_Pin_12
	 | GPIO_Pin_13
	 | GPIO_Pin_14
	 | GPIO_Pin_15;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	 GPIO_Init(GPIOB, &GPIO_InitStructure);
	 */
	/*
	 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_13
	 | GPIO_Pin_14
	 | GPIO_Pin_15;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	 GPIO_Init(GPIOB, &GPIO_InitStructure);
	 */
}

void SetupUSART() {
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	/* Enable GPIOA clock                                                   */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1,
			ENABLE);
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_GPIOA | RCC_APB2Periph_USART1 | RCC_APB1Periph_USART2, ENABLE);
	//RCC_APB1PeriphClockCmd(RCC_APB1Peri)

	/* Configure USART1 Rx (PA10) as input floating                         */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure USART1 Tx (PA9) as alternate function push-pull            */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	/*
	 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_3;
	 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
	 GPIO_Init(GPIOA, &GPIO_InitStructure);

	 GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
	 GPIO_Init(GPIOA, &GPIO_InitStructure);
	 */

	/* USART1 configured as follow:
	 - BaudRate = 115200 baud
	 - Word Length = 8 Bits
	 - One Stop Bit
	 - No parity
	 - Hardware flow control disabled (RTS and CTS signals)
	 - Receive and transmit enabled
	 - USART Clock disabled
	 - USART CPOL: Clock is active low
	 - USART CPHA: Data is captured on the middle
	 - USART LastBit: The clock pulse of the last data bit is not output to
	 the SCLK pin
	 */

	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
			USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	USART_Cmd(USART1, ENABLE);
	/* Enable RXNE interrupt */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
//      USART_ITConfig(USART1, USART_IT_TC, ENABLE);
	NVIC_EnableIRQ(USART1_IRQn);
	/*
	 USART_InitStructure.USART_BaudRate            = 115200;
	 USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
	 USART_InitStructure.USART_StopBits            = USART_StopBits_1;
	 USART_InitStructure.USART_Parity              = USART_Parity_No ;
	 USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	 USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
	 USART_Init(USART2, &USART_InitStructure);
	 USART_Cmd(USART2, ENABLE);
	 */
	serialPortMutex = xSemaphoreCreateMutex();
}

/*******************************************************************************
 * Function Name  : SendChar
 * Description    : Send a char by USART1
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
int SendChar(int ch) /* Write character to Serial Port     */
{
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
		;
	USART_SendData(USART1, ch);
	return (ch);
}

/*******************************************************************************
 * Function Name  : uart_printf
 * Description    : Send a string by USART1
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void uart_printf(char *ptr) {
	if (xSemaphoreTake(serialPortMutex, 10000)) {
		while (*ptr) {
			SendChar(*ptr);
			ptr++;
		}
		xSemaphoreGive(serialPortMutex);
	}
}

/**
 *********************************************************************************************
 * @brief		Send a string by USART
 *
 * @param[in] 	dat		Data which be sent.
 * @param[in] 	ct		How long will be print.
 * @param[out] 	None
 * @retval		None
 * @details		This function is called to print string with 'ct' bit hex format by USART.
 *********************************************************************************************
 */
void uart_print_hex(unsigned int dat, char ct) {
	char i, count;
	unsigned int ddat;
	char ptr[8], tmp;
	ddat = dat;
	count = 0;
	while (ddat != 0) {
		tmp = ddat % 16;
		ptr[count] = tmp > 9 ? ('A' + tmp - 10) : (tmp + '0');
		ddat = ddat / 16;
		count++;
	}

	if (count >= ct)
		count = ct;
	else {
		for (i = 0; i < ct - count; i++) {
			SendChar('0');
		}
	}

	for (i = count; i > 0; i--) {
		SendChar(ptr[i - 1]);
	}
}
