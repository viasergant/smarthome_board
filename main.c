#include "AppConfig.h"
#include "xhw_types.h"
#include "xhw_memmap.h"
#include "xsysctl.h"
//#include "xgpio.h"
#include "stm32f10x_iwdg.h"
#include "ds18b20.h"
#include <string.h>
#include <stdlib.h>

#define ANALOG_SENSORS 1
#define SENSORS_LOOP_DELAY 10000
#define WAIT_SUCCESS_SENSORS 60000 // 1 minute
#define WAIT_SUCCESS_HOST    1200000 // 20 minutes
#define HOUR_MILLIS          3600000 // 1 hour
#define TEMP_SENSORS_MAX	 15
int analogSensors[ANALOG_SENSORS] = { 1 };

char endLine0 = ';';
char endLine1 = '\n';
char endLine2 = '\r';
char endLine3 = '\r\n';
char address_array[TEMP_SENSORS_MAX][8];
int temperatureSensorsCount = 0;

typedef struct {
	char prefix[1 + 1];
	unsigned char sensorIndex;
	int value;
	char addr[8];
} TempMessage;

QueueHandle_t tempQ = 0;
QueueHandle_t uartRecvQ = 0;

char inputString[64];
char sendBuffer[256];
char *pInputString;
unsigned char stringComplete = 0;
unsigned char debug = 1;
unsigned char valveOn = 0;
unsigned char valveOff = 0;
unsigned char reset_every_hour = 0;
unsigned long lastSuccessHostUpdate = 0;

unsigned char a11 = 0;
unsigned char a12 = 0;

char *getValue(char *data, char separator, int index, char *buffer) {
	int found = 0;
	int strIndex[] = { 0, -1 };
	int maxIndex = strlen(data) - 1;
	int i;
	buffer[0] = '\0';
	for (i = 0; i <= maxIndex && found <= index; i++) {
		if (data[i] == separator || i == maxIndex) {
			found++;
			strIndex[0] = strIndex[1] + 1;
			strIndex[1] = (i == maxIndex) ? i + 1 : i;
		}
	}
	if (found > index) {
		memcpy(buffer, data + strIndex[0], strIndex[1] - strIndex[0]);
		buffer[strIndex[1] - strIndex[0]] = '\0';
	} else {
		buffer[0] = '\0';
	}
	return buffer;
}

void software_Reboot() {
	NVIC_SystemReset();
}

void reset_host() {
	uart_printf("MSG=resetting host - start\r\n");
	vTaskDelay(5000);
	GPIO_ResetBits(GPIOC, 1 << 14);
	SetPortDirectionOut(GPIOC, 14);
	uart_printf("MSG=resetting host - start\r\n");
	vTaskDelay(5000);
	uart_printf("MSG=resetting host - end\r\n");
	GPIO_SetBits(GPIOC, 1 << 14);
	SetPortDirectionIn(GPIOC, 14, -1);
	lastSuccessHostUpdate = xTaskGetTickCount();
}

void initPeripheral() {
	SetupGPIO();
	SetupUSART();
	InitADC();
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable); //Enable write PR and RLR
	IWDG_SetPrescaler(IWDG_Prescaler_256); //Write PR pre frequency value
	IWDG_SetReload(4095); //Write RLR 32s
	IWDG_Enable(); //KR write 0xCCCC

	//RCC_ClocksTypeDef RCC_Clocks;
	//RCC_GetClocksFreq(&RCC_Clocks);
	//SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);
}

void USART1_IRQHandler() {
	portBASE_TYPE xTaskWoken;
	/*
	 if (USART_GetITStatus(USART1, USART_IT_TC) != RESET) {
	 xSemaphoreGiveFromISR(g_uartSendSemaphore, &xTaskWoken);
	 uint16_t inp = ';';
	 xQueueSendFromISR( uartRecvQ, &inp, NULL);
	 }
	 */
	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
		uint16_t inp = USART_ReceiveData(USART1);

		xQueueSendFromISR( uartRecvQ, &inp, NULL);
	}
	//USART_ClearITPendingBit(USART1, USART_IT_TC);
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
}

void pinSet() {
}

void copy_rom(char *src, char *dst) {
	int i;
	for (i = 0; i < 8; i++) {
		dst[i] = src[i];
	}
}

void addTempSensorToArray(char *textAddr) {
	char byteContent[8];
	char convBuf[3];
	int i;
	int found=0;
	for (i=0;i<8;i++) {
		memcpy(convBuf,textAddr + (i*2),2);
		convBuf[2] = 0;
		byteContent[i] = strtol(convBuf, NULL, 16);
	}

	for (i=0;i<temperatureSensorsCount && !found;i++) {
		if (memcmp(byteContent,address_array[i],8) == 0) {
			found = 1;
		}
	}

	if (!found) {
		memcpy(address_array[temperatureSensorsCount],byteContent,8);
		temperatureSensorsCount++;
		uart_printf("MSG=added new temperature sensor\r\nMSG=");
		for (i = 0; i < 8; i++) {
			uart_print_hex(address_array[temperatureSensorsCount-1][i], 2);
		}
		uart_printf("\r\n");
	} else {
		uart_printf("MSG=sensor not added because it exists already!\r\n");
	}
}

void processCommand(char *command) {
	int pin;
	int allow_process = 1;
	char strBuffer[64];

	if (debug) {
		sprintf(sendBuffer, "MSG=Accepted: %s\r\n", command);
		uart_printf(sendBuffer);
	}

	if (strcmp(getValue(command, ':', 0, strBuffer), "P") == 0) {
		char *pinStr = getValue(command, ':', 1, strBuffer);

		char port = pinStr[0];
		unsigned char pin = atoi(&pinStr[1]);
		GPIO_TypeDef *portBase;
		if (port == 'a' || port == 'A') {
			portBase = GPIOA;
		} else if (port == 'b' || port == 'B') {
			portBase = GPIOB;
		} else if (port == 'c' || port == 'C') {
			portBase = GPIOC;
		} else {
			portBase = GPIOA;
		}

		int val = atoi(getValue(command, ':', 2, strBuffer));

		if (val == 1 && pin == 11 && portBase == GPIOA && a12) {
			if (debug) {
				uart_printf("MSG=pin12 is set. Attempt to set pin11\r\n");
			}
			allow_process = 0;
		}

		if (val == 1 && pin == 12 && portBase == GPIOA && a11) {
			if (debug) {
				uart_printf("MSG=pin11 is set. Attempt to set pin12\r\n");
			}
			allow_process = 0;
		}

		if (allow_process) {
			if (val == 0) {
				if (pin == 11 && portBase == GPIOA)
					a11 = 0;
				if (pin == 12 && portBase == GPIOA)
					a12 = 0;

				GPIO_ResetBits(portBase, 1 << pin);
			}

			if (val == 1) {
				if (pin == 11 && portBase == GPIOA)
					a11 = 1;
				if (pin == 12 && portBase == GPIOA)
					a12 = 1;

				GPIO_SetBits(portBase, 1 << pin);
			}
			SetPortDirectionOut(portBase, pin);
			sprintf(sendBuffer, "O=%c%d;%d\r\n", port, pin, val);
			uart_printf(sendBuffer);

			/*
			 if (val == 2) {
			 if (debug) {
			 Serial.print("MSG=pullup pin: ");
			 Serial.print(pin);
			 Serial.println();
			 }
			 pinMode(pin, INPUT_PULLUP);
			 }
			 */
		}
	} else if (strcmp(getValue(command, ':', 0, strBuffer), "I") == 0) {
		char *pinStr = getValue(command, ':', 1, strBuffer);
		char port = pinStr[0];
		unsigned char pin = atoi(&pinStr[1]);
		GPIO_TypeDef *portBase;
		if (port == 'a' || port == 'A') {
			portBase = GPIOA;
		} else if (port == 'b' || port == 'B') {
			portBase = GPIOB;
		} else if (port == 'c' || port == 'C') {
			portBase = GPIOC;
		} else {
			portBase = GPIOA;
		}

		int pull = atoi(getValue(command, ':', 2, strBuffer));
		SetPortDirectionIn(portBase, pin, pull);

		u8 ReadValue = GPIO_ReadInputDataBit(portBase, 1 << pin);
		sprintf(sendBuffer, "O=%c%d;%d\r\n", port, pin, ReadValue);
		uart_printf(sendBuffer);

	} else if (strcmp(getValue(command, ':', 0, strBuffer), "RESET_BOARD")
			== 0) {
		uart_printf("MSG=REBOOT...\r\n");
		vTaskDelay(1000);
		software_Reboot();
	} else if (strcmp(getValue(command, ':', 0, strBuffer), "D") == 0) {
		debug = atoi(getValue(command, ':', 1, strBuffer));
	} else if (strcmp(getValue(command, ':', 0, strBuffer), "ADDT") == 0) {
		addTempSensorToArray(getValue(command, ':', 1, strBuffer));
	} else if (strcmp(getValue(command, ':', 0, strBuffer), "R") == 0) {
		reset_every_hour = atoi(getValue(command, ':', 1, strBuffer));
	} else if (strcmp(getValue(inputString, ':', 0, strBuffer), "HEART_BEAT")
			== 0) {
		lastSuccessHostUpdate = xTaskGetTickCount();
	}
}

void vProcessUARTRecvTask(void *param) {
	uint16_t recvChar;

	for (;;) {
		if (xQueueReceive(uartRecvQ, &recvChar, 10000)) {
			char inChar = (char) recvChar;
			if ((inChar == endLine1) || (inChar == endLine0)) //\n
					{
				stringComplete = 1;
				processCommand(pInputString);
				pInputString[0] = '\0';
			} else {
				pInputString = strncat(pInputString, &inChar, 1);
			}
		}
	}
}

void InitADC() {
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);

	// настройки ADC
	ADC_InitTypeDef ADC_InitStructure;
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; // режим работы - одиночный, независимый
	ADC_InitStructure.ADC_ScanConvMode = DISABLE; // не сканировать каналы, просто измерить один канал
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; // однократное измерение
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; // без внешнего триггера
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; //выравнивание битов результат - прижать вправо
	ADC_InitStructure.ADC_NbrOfChannel = 1; //количество каналов - одна штука
	ADC_Init(ADC2, &ADC_InitStructure);
	ADC_Cmd(ADC2, ENABLE);

	// настройка канала
	ADC_RegularChannelConfig(ADC2, ADC_Channel_8, 1, ADC_SampleTime_55Cycles5);

	// калибровка АЦП
	ADC_ResetCalibration(ADC2);
	while (ADC_GetResetCalibrationStatus(ADC2))
		;
	ADC_StartCalibration(ADC2);
	while (ADC_GetCalibrationStatus(ADC2))
		;
}

void vReadTemperatureTask(void *param) {
	char tBuf[50];
	tDS18B20Dev dev;
	dev.ulPort = GPIOB; //xGPIO_PORTC_BASE;
	dev.ulPin = GPIO_Pin_7;
	dev.PinSet = pinSet;
	TempMessage msg;
	int currentSensor = 0;
	uart_printf("MSG=Search DS18B20...\r\n");
	vTaskSuspendAll();

	DS18B20Init(&dev);

	while (DS18B20ROMSearch(&dev)) {
		int i;
		DS18B20ScratchpadSet(&dev, 100, 0, DS18B20_11BIT);
		uart_printf("MSG=");
		for (i = 0; i < 8; i++) {
			address_array[temperatureSensorsCount][i] = dev.ucROM[i];
			uart_print_hex(dev.ucROM[i], 2);
		}
		uart_printf("\r\n");
		temperatureSensorsCount++;
	}
	xTaskResumeAll();

	for (;;) {
		IWDG_ReloadCounter();
		if (currentSensor >= temperatureSensorsCount + ANALOG_SENSORS) {
			currentSensor = 0;
		}
		if (currentSensor < temperatureSensorsCount) {
			vTaskSuspendAll();
			int cnt = 0;
			DS18B20Reset(&dev);
			copy_rom(&(address_array[currentSensor][0]), &(dev.ucROM[0]));
			DS18B20ROMMatch(&dev);
			DS18B20TempConvert(&dev);
			xTaskResumeAll();

			vTaskDelay(1000);
			float temp = 0.0;

			vTaskSuspendAll();
			DS18B20Reset(&dev);
			DS18B20ROMMatch(&dev);
			DS18B20TempRead(&dev, &temp);
			xTaskResumeAll();

			msg.value = temp * 1000.0;
			copy_rom(&dev.ucROM[0], &msg.addr[0]);
			msg.prefix[0] = 'T';
			msg.prefix[1] = '\0';
			xQueueSend(tempQ, (void*)&msg, 0);
		} else {
			msg.value = getAdcValue();
			msg.prefix[0] = 'P';
			msg.prefix[1] = '\0';
			msg.sensorIndex = currentSensor - temperatureSensorsCount;
			xQueueSend(tempQ, (void*)&msg, 0);
		}
		currentSensor++;

		vTaskDelay(10000);
	}
}

void vToolTask(void *param) {
	lastSuccessHostUpdate = xTaskGetTickCount();
	for (;;) {
		if ((xTaskGetTickCount() - lastSuccessHostUpdate) > WAIT_SUCCESS_HOST) {
			reset_host();
			software_Reboot();
		}
		vTaskDelay(60000);
	}
}

void vSendToUARTTask(void *param) {
	char tBuf[50];
	TempMessage msg;
	for (;;) {
		int temp;
		if (xQueueReceive(tempQ, &msg, 10000)) {
			int i;
			if (msg.prefix[0] == 'T') {
				uart_printf("T=");
				for (i = 0; i < 8; i++) {
					uart_print_hex(msg.addr[i], 2);
				}
				sprintf(tBuf, ";%d\r\n", msg.value);
				uart_printf(tBuf);
			} else if (msg.prefix[0] == 'P') {
				sprintf(tBuf, "P=%d;%d\r\n", msg.sensorIndex, msg.value);
				uart_printf(tBuf);
			}
		}
	}
}

void setup(void) {
	char tBuf[64];
	initPeripheral();
	inputString[0] = '\0';
	pInputString = &inputString[0];

	uart_printf("\r\nMSG=System initial...                  \r\n");

	uart_printf("MSG=System Clock have been configured as ");
	sprintf(tBuf, "%dMHz!\r\n", (int) xSysCtlClockGet() / 1000000);
	uart_printf(tBuf);

	tempQ = xQueueCreate(10, sizeof(TempMessage));
	uartRecvQ = xQueueCreate(50, sizeof(uint16_t));

	xTaskCreate( &vReadTemperatureTask, (signed char *)"ReadTemperature",
			configMINIMAL_STACK_SIZE, NULL, 1, NULL);
	xTaskCreate( &vProcessUARTRecvTask, (signed char *)"ProcessUARTRecvTask",
			configMINIMAL_STACK_SIZE, NULL, 1, NULL);
	xTaskCreate( &vSendToUARTTask, (signed char *)"SendToUART",
			configMINIMAL_STACK_SIZE, NULL, 1, NULL);

	xTaskCreate( &vToolTask, (signed char *)"ToolTask",
			configMINIMAL_STACK_SIZE, NULL, 1, NULL);

	uart_printf("MSG=Start multi-task.                   \r\n");
	vTaskStartScheduler();
	uart_printf("END MULTI-TASK.");
}

int main(void) {
	setup();
	while (1)
		;
}
