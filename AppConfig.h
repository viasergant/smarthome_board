#ifndef _APP_CONFIG_H
#define _APP_CONFIG_H

#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_adc.h"
#include "core_cm3.h"
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>

#define STM32F1xx

//#define OS_EVT U16

//void GPIO_Configuration (void);
void UART_Configuration (void);
//void LCD_Configuration  (void);
//void ADC_Configuration  (void);
//void RTC_Configuration  (void);
//void EXIT_Configuration (void);
//void NVIC_Configuration (void);
void SysTick_Configuration (void);
void SetupUSART();
void SetupGPIO();
void SetupClock();
uint16_t getAdcValue();
void SetPortDirectionIn(GPIO_TypeDef* port, uint16_t pin, int pull );
void SetPortDirectionOut(GPIO_TypeDef* port, uint16_t pin);
//void DisableRTCInterrupt(void);
//void EnableRTCInterrupt	(void);

void uart_printf (char *ptr);
void uart_print_hex (unsigned int dat,char ct);

#endif
